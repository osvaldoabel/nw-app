Temos neste diretório o básico para rodar este projeto utilizando containers docker.

1) Instale o docker
    https://docs.docker.com/install/

2) Dentro deste diretório execute:
    docker-compose up

- Iniciar os containers:
    docker-compose start

- Parar os containers:
    docker-compose stop

Entrar em algum container:
    docker exec -it [nome_do_container] bash
    Exemplo: 
    docker exec -it web bash (Para entrar no container 'web')
