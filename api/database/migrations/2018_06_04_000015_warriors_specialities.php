<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class WarriorsSpecialities extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('speciality_warrior', function(Blueprint $table)
        {
            $table->integer('warrior_id')->unsigned();
            $table->foreign('warrior_id')->references('id')
                  ->on('warriors')
                  ->onDelete('cascade');
      
            $table->integer('speciality_id')->unsigned();
            $table->foreign('speciality_id')
                  ->references('id')
                  ->on('specialities')
                  ->onDelete('cascade');
      
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('warriors_specialities');
    }
}
