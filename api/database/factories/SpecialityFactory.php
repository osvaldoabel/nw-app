<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Speciality::class, function (Faker $faker) {
    return [
        'name' => $faker->firstName,
        'status' => 1
    ];
});
