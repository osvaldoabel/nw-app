<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Warrior::class, function (Faker $faker) {
    return [
        'name' => $faker->firstName,
        'status' => 1,
        'life' => rand(100, 500),
        'damnification' => rand(10, 50),
        'defense' => rand(1, 100),
        'attack_velocity' => 100,
        'movement_velocity' => rand(1, 50),
        'category_id' => rand(1, 4),
    ];
});