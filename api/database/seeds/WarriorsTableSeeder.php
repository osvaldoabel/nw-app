<?php

use Illuminate\Database\Seeder;
use \App\Models\Thumbnail;
use App\Models\Warrior;
use Faker\Factory;

class WarriorsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $self = $this;

        \File::deleteDirectory(storage_path('app'), true);

        factory(Warrior::class, 5)
                ->create()
                ->each(function($warrior) use ($self){
                    $self->generateThumbnails($warrior);
                });
    }

    private function generateThumbnails(Warrior $warrior)
    {
        $warriorDir = storage_path("app/{$warrior->id}");
        \File::makeDirectory($warriorDir);
        $faker = Factory::create();

        factory(Thumbnail::class, 3)
        ->make()
        ->each(function($thumbnail) use ($warrior, $faker, $warriorDir) {
            $thumbnail->warrior_id = $warrior->id;
            $thumbnail->filename  = $faker->image($warriorDir, 640, 480, 'people', false);
            
            $thumbnail->principal = 1;

            if ($warrior->thumbnails->first()) {
                $thumbnail->principal = 0;
            }

            $thumbnail->description = $faker->text;
            $thumbnail->status      = 1;
            $thumbnail->save();
        });
    }
}
