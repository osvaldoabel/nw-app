<?php

use Illuminate\Database\Seeder;
use App\Models\Speciality;
class SpecialitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Speciality::class, 5)->create();
    }
}
