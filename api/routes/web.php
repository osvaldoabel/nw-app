<?php

Route::get('/', function () {
    return view('spa');
});

Route::name('thumbnail.url')->get('/download/thumbnails/{id}', 'ThumbnailController@download');