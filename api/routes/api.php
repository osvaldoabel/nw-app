<?php

Route::resource('categories',  'CategoryController');
Route::resource('warriors',  'WarriorController');
Route::resource('specialities', 'SpecialityController');
// Route::resource('specialities.warriors', 'WarriorController');
Route::resource('warriors.thumbnails',  'ThumbnailController');

Route::get('/specialities/{id}/warriors', 'WarriorController@getBySpecialisty');
