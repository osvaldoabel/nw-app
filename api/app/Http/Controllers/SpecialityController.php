<?php

namespace App\Http\Controllers;

use App\Repositories\Specialities\SpecialityRepositoryInterface;
use App\Http\Resources\SpecialityResource;
use App\Common\ControllerTrait;

use Illuminate\Http\Request;

class SpecialityController extends Controller
{
    use ControllerTrait;

    protected $items;

    public function __construct(SpecialityRepositoryInterface $specialities)
    {
        $this->items    = $specialities;
        $this->resource = SpecialityResource::class;
    }

}
