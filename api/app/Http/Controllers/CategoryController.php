<?php

namespace App\Http\Controllers;

use App\Repositories\Categories\CategoryRepositoryInterface;
use App\Http\Resources\CategoryResource;
use App\Common\ControllerTrait;

use Illuminate\Http\Request;

class CategoryController extends Controller
{
    use ControllerTrait;
    protected $categories;

    public function __construct(CategoryRepositoryInterface $categories)
    {
        $this->items    = $categories;
        $this->resource = CategoryResource::class;
    }

}
