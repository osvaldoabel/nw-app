<?php

namespace App\Http\Controllers;
use App\Models\Warrior;
use App\Models\Thumbnail;
use Illuminate\Http\Request;
use App\Repositories\Thumbnails\ThumbnailRepositoryInterface;
use App\Http\Resources\ThumbnailResource;
use App\Common\ControllerTrait;

class ThumbnailController extends Controller
{
    use ControllerTrait;

    protected $items;
    protected $thumbnails;
    
    public function __construct(ThumbnailRepositoryInterface $thumbnails)
    {
        $this->items      = $thumbnails;
        $this->resource   = ThumbnailResource::class;
    }

    public function index(Warrior $warrior)
    {
        $thumbnails = $this->items->all($warrior);
        return $this->resource::collection($thumbnails);
    }

    public function store(Request $request, Warrior $warrior)
    {
        $thumbnail = $this->items->store($request, $warrior);
        return new $this->resource($thumbnail);
    } 

    public function update(Request $request, Warrior $warrior, Thumbnail $thumbnail)
    {
        $data       = $request->all();
        $thumbnail  = $this->items->update($thumbnail, $data);

        return new $this->resource($thumbnail);
    }

    public function download(int $id)
    {
        $thumbnailPath = $this->items->getPath($id);
        return response()->download($thumbnailPath);
    }
    
    public function ThumbnailUrl($name)
    {
        $thumbnails = $this->items->whereFileName($name)->get();
        
        if (!$thumbnails->count()) {
            abort(404);
        }

        $thumbnail      = $thumbnails->first();
        $thumbnailPath  =  storage_path("app/{$thumbnail->Warrior_id}/$thumbnail->filename");

        return response()->download($thumbnailPath);
    }

}
