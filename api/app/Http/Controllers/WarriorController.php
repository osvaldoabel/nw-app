<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Common\ControllerTrait;
use Illuminate\Database\Eloquent\Collection;
// use App\Models\Speciality;

use App\Repositories\Warriors\WarriorRepositoryInterface;
use App\Repositories\Specialities\SpecialityRepositoryInterface;
use App\Repositories\Thumbnails\ThumbnailRepositoryInterface;

use App\Http\Resources\WarriorResource;

class WarriorController extends Controller
{
    use ControllerTrait;

    protected $items;
    protected $specialities;

    public function __construct(
        WarriorRepositoryInterface $warriors,
        SpecialityRepositoryInterface $specialities,
        ThumbnailRepositoryInterface $thumbnails )
        {
            $this->items        = $warriors;
            $this->thumbnails   = $thumbnails;
            $this->specialities = $specialities;
            $this->resource     = WarriorResource::class;
    }

    public function getBySpecialisty(int $id) : Collection
    {
        $speciality = $this->specialities->find($id);
        
        if (!$speciality) {
            abort(404);
        }

        return $speciality->warriors;        
    }

    /**
     * Undocumented function
     *
     * @param integer $id id da Speciality
     * @return void
     */
    public function index()
    {
        $items  = $this->items->all();
        return $this->resource::collection($items);
    }

    protected function getInputs(Request $request)
    {
        return $request->all();
    }
    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->items->validate($request);

        $inputs  = $this->getInputs($request);

        $warrior = $this->items->store($inputs);

        $this->thumbnails->store($request, $warrior);

        return new $this->resource($warrior);
    }

    public function update(Request $request, $id)
    {
        $this->items->validate($request, 'edit');
        $inputs  = $this->getInputs($request);

        $warrior = $this->items->update($id, $inputs);

        return new $this->resource($warrior);
    }

}
