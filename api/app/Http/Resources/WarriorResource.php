<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class WarriorResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        return [
            'id'                => $this->id,
            'name'              => $this->name,
            'category'          => $this->category()->first(),
            'specialities'      => $this->specialities()->get(),
            'thumbnails'        => ThumbnailResource::collection($this->thumbnails),
            'thumbnail'         => $this->getPrincipalThumbnail($this->thumbnails),
            'life'              => $this->life,
            'damnification'     => $this->damnification,
            'defense'           => $this->defense,
            'attack_velocity'   => $this->attack_velocity,
            'movement_velocity' => $this->movement_velocity,
            'status'            => $this->status,
            'created_at'        => $this->created_at,
            'updated_at'        => $this->updated_at
        ];
    }

    /**
     * Recebe uma lista de Thumbnails e retorna o que 
     * está selecionado como principal
     *
     * @param [type] $thumbnails
     * @return void
     */
    private function getPrincipalThumbnail($thumbnails)
    {
        $thumbnail = $thumbnails->firstWhere('principal', '=', 1);
        return $thumbnail
                ? new ThumbnailResource($thumbnail)
                : [];
    }
}










