<?php

namespace App\Repositories\Categories;
use DB;
use Validator;
use App\Common\RepositoryTrait;
use App\Models\Category;

class CategoryRepository implements CategoryRepositoryInterface
{
    use RepositoryTrait;

    public $model;

	public function __construct(Category $categories)
	{
        $this->model = $categories; 
	}
}