<?php

namespace App\Repositories\Thumbnails;

use Validator;
use App\Models\Warrior;
use App\Models\Thumbnail;
use App\Common\RepositoryTrait;
use App\Http\Requests\UploadRequest;

class ThumbnailRepository implements ThumbnailRepositoryInterface
{
    use RepositoryTrait;

    public $model;

	public function __construct(Thumbnail $thumbnails)
	{
        $this->model = $thumbnails;
    }

    /**
     * Undocumented function
     *
     * @param integer $principal
     * @param [type] $warrior
     * @param [type] $file
     * @return void
     */
    protected function createSingleThumbnail($principal = 0, $warrior, $file)
    {
        $file->store("$warrior->id");

        return $this->model->create( [
            'description' => '',
            'principal' => $principal,
            'warrior_id'=> $warrior->id,
            'filename'  => $file->hashName(),
            'status'    => 1
        ]);
    }
    
    /**
     * Undocumented function
     *
     * @param [type] $request
     * @param [type] $warrior
     * @return void
     */
    public function store($request, $warrior)
    {
        $count  = 0;
        $arr    = [];
        
        foreach ($request->thumbnails as $file) {
            $principal = (bool) $count;
            $thumbnail = $this->createSingleThumbnail($principal, $warrior, $file);

            $warrior->thumbnails()->save($thumbnail);
            $count++;
        }

        return $warrior->thumbnails;
    }

    /**
     * Undocumented function
     *
     * @param [type] $id
     * @return string|null
     */
    public function getPath($id) : ?string
    {
       $thumbnail = $this->model->find($id);

       if (!$thumbnail->count()) {
            return null;
        }

        return storage_path("app/{$thumbnail->warrior_id}/$thumbnail->filename");
    }

    /**
     * Undocumented function
     *
     * @param [type] $warrior
     * @return void
     */
    public function all($warrior)
    {
        return $warrior->thumbnails;
    }
}