<?php

namespace App\Repositories\Specialities;
use DB;
use Validator;

use App\Models\Speciality;
use App\Common\RepositoryTrait;

class SpecialityRepository implements SpecialityRepositoryInterface
{
    use RepositoryTrait;

    public $model;

	public function __construct(Speciality $specialities)
	{
        $this->model = $specialities; 
	}

}