<?php

namespace App\Repositories\Warriors;
use DB;
use Validator;

use App\Models\Warrior;
use App\Common\RepositoryTrait;

class WarriorRepository implements WarriorRepositoryInterface
{
    use RepositoryTrait;
    
    protected $model;
    protected $rules;

	public function __construct(Warrior $warriors)
	{
        $this->model = $warriors;
        $this->rules = $this->getRules();
    }

    /**
     * Undocumented function
     *
     * @param string $action
     * @return void
     */
    public function getRules(string $action = null)
    {
        if (!$action) {
            return [
                'name'      => 'required',
                'category_id' => 'required|int',
                'movement_velocity' => 'required',
                'damnification' => 'required',
                'attack_velocity' => 'required',
                'defense'   => 'required',
                'status'    => 'required',
                'specialities' => 'required',
            ];
        }

        return [];
    }

    public function validate($request, $action = null)
    {
        $rules = $this->getRules($action);
        return $request->validate($rules);       
    }
    
    /**
     * {@inheritDoc}
     */
    public function update($id, array $data)
    {
        $model = $this->model->find($id);
        $model->fill($data);
        $model->save();
        
        $speciality_ids = $this->extract('id', $data['specialities']);

        if (count($speciality_ids)) {
            $model->specialities()->sync($speciality_ids);
        }

        return $model;
    }

    /**
     * Undocumented function
     *
     * @param array $data
     * @return Warrior
     */
    public function store($data) : Warrior
    {
        $warrior = $this->model->fill($data);
        $warrior->save();

        $speciality_ids = $this->extract('id', explode(',', $data['specialities']));
        $warrior->specialities()->sync($speciality_ids);

        return $warrior;
    }

    public function all($filter = [])
    {
        return $this->model
                    ->with('thumbnails', 'category')
                    ->orderBy('updated_at', 'desc')
                    ->get();
    }

}