<?php

namespace App\Common;
use DB;
use Validator;

trait RepositoryTrait
{
    /**
     * {@inheritDoc}
     */
    public function save(array $data)
    {
        $model = $this->model->fill($data);
        $model->save();

        return $model;
    }
    
    protected function extract($field, $collection)
    {
        $result = array_map(function($item) {
            if (is_array($item)) {
                return ($item['id']);
            }

            return $item;
        },$collection);

        return array_filter($result);
    }

    /**
     * {@inheritDoc}
     */
    public function store(array $data)
    {
        $model = $this->model->fill($data);

        $model->save();

        return $model;
    }

    /**
     * {@inheritDoc}
     */
    public function update($id, array $data)
    {
        $model = $this->model->find($id);

        $model->fill($data);
        $model->save();

        return $model;
    }

    public function find($id)
    {
        return $this->model->find($id);
    }

    public function destroy($model)
    {
        try {
            $result = $model->delete();
        } catch (\Exception $e) {
            $result  = false;
        }

        return $result;
    }

    public function all()
    {
        return $this->model->orderBy('created_at', 'desc')->get();
    }
}