<?php

namespace App\Common;

use Validator;
use Illuminate\Http\Request;

trait ControllerTrait
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items  = $this->items->all();
        return $this->resource::collection($items);
    }

    public function show($id)
    {
        $item = $this->items->find($id);

        if (!$item) {
            abort(404);
        }

        return new $this->resource($item);
    }

    public function destroy($id)
    {
        $item = $this->items->find($id);

        if (!$item) {
            abort(404);
        }

        try {
            $this->items->destroy($item);

            return [
                'message'       => "Excluido com Sucesso!",
                'status-code'   => 200
            ];

        } catch(\Exception $e) {
            $response['status-code'] = 409;
            $response['message']     = "Erro ao Deletar o Objeto #$item->id";

            return $response;
        }       

        return $response;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data  = $request->all();
        $item  = $this->items->store($data);
        return new $this->resource($item);
    }
    
    public function update(Request $request, $id)
    {
        $data  = $request->all();
        $model = $this->items->update($id, $data);

        return new $this->resource($item);
    }
}