<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Warrior extends Model
{
    protected $fillable = [
        'name',
        'category_id',
        'life',
        'damnification',
        'defense',
        'attack_velocity',
        'movement_velocity',
        'status'
    ];

    public function thumbnails()
    {
        return $this->hasMany(Thumbnail::class);
    }

    public function specialities()
    {
        return $this->belongsToMany(Speciality::class);
    }
    
    public function category()
    {
        return $this->belongsTo(Category::class);
    }

}