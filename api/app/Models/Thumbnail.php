<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Thumbnail extends Model
{
    protected $fillable = [
        'warrior_id',
        'principal',
        'filename',
        'description',
        'status'
    ];
}
