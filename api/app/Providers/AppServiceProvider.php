<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'App\Repositories\Categories\CategoryRepositoryInterface',
            'App\Repositories\Categories\CategoryRepository'
        );

        $this->app->bind(
            'App\Repositories\Specialities\SpecialityRepositoryInterface',
            'App\Repositories\Specialities\SpecialityRepository'
        );

        $this->app->bind(
            'App\Repositories\Thumbnails\ThumbnailRepositoryInterface',
            'App\Repositories\Thumbnails\ThumbnailRepository'
        );

        $this->app->bind(
            'App\Repositories\Warriors\WarriorRepositoryInterface',
            'App\Repositories\Warriors\WarriorRepository'
        );
    }
}
