(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error('Cannot find module "' + req + '".');
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n  <router-outlet></router-outlet>\n</div>"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var AppComponent = /** @class */ (function () {
    function AppComponent() {
        this.title = 'app';
    }
    AppComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _components_warrior_list_warrior_list_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./components/warrior-list/warrior-list.component */ "./src/app/components/warrior-list/warrior-list.component.ts");
/* harmony import */ var _components_warrior_create_warrior_create_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./components/warrior-create/warrior-create.component */ "./src/app/components/warrior-create/warrior-create.component.ts");
/* harmony import */ var _services_warriors_warrior_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./services/warriors/warrior.service */ "./src/app/services/warriors/warrior.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _components_warrior_show_warrior_show_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./components/warrior-show/warrior-show.component */ "./src/app/components/warrior-show/warrior-show.component.ts");
/* harmony import */ var _components_thumbnail_list_thumbnail_list_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./components/thumbnail-list/thumbnail-list.component */ "./src/app/components/thumbnail-list/thumbnail-list.component.ts");
/* harmony import */ var _angular_material_select__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/material/select */ "./node_modules/@angular/material/esm5/select.es5.js");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm5/animations.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};













var routes = [
    { path: '', component: _components_warrior_list_warrior_list_component__WEBPACK_IMPORTED_MODULE_3__["WarriorListComponent"] },
    { path: 'warriors', component: _components_warrior_list_warrior_list_component__WEBPACK_IMPORTED_MODULE_3__["WarriorListComponent"] },
    { path: 'warriors/:id/show', component: _components_warrior_show_warrior_show_component__WEBPACK_IMPORTED_MODULE_9__["WarriorShowComponent"] },
    { path: 'warriors/create', component: _components_warrior_create_warrior_create_component__WEBPACK_IMPORTED_MODULE_4__["WarriorCreateComponent"] },
    { path: 'warriors/:id/edit', component: _components_warrior_create_warrior_create_component__WEBPACK_IMPORTED_MODULE_4__["WarriorCreateComponent"] }
];
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_2__["AppComponent"],
                _components_warrior_list_warrior_list_component__WEBPACK_IMPORTED_MODULE_3__["WarriorListComponent"],
                _components_warrior_create_warrior_create_component__WEBPACK_IMPORTED_MODULE_4__["WarriorCreateComponent"],
                _components_warrior_show_warrior_show_component__WEBPACK_IMPORTED_MODULE_9__["WarriorShowComponent"],
                _components_thumbnail_list_thumbnail_list_component__WEBPACK_IMPORTED_MODULE_10__["ThumbnailListComponent"]
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_6__["RouterModule"].forRoot(routes, { useHash: true }),
                _angular_common_http__WEBPACK_IMPORTED_MODULE_7__["HttpClientModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_8__["FormsModule"],
                _angular_material_select__WEBPACK_IMPORTED_MODULE_11__["MatSelectModule"],
                _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_12__["BrowserAnimationsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_8__["ReactiveFormsModule"]
            ],
            providers: [_services_warriors_warrior_service__WEBPACK_IMPORTED_MODULE_5__["WarriorService"]],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_2__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/components/thumbnail-list/thumbnail-list.component.css":
/*!************************************************************************!*\
  !*** ./src/app/components/thumbnail-list/thumbnail-list.component.css ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/components/thumbnail-list/thumbnail-list.component.html":
/*!*************************************************************************!*\
  !*** ./src/app/components/thumbnail-list/thumbnail-list.component.html ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\n  thumbnail-list works!\n</p>\n"

/***/ }),

/***/ "./src/app/components/thumbnail-list/thumbnail-list.component.ts":
/*!***********************************************************************!*\
  !*** ./src/app/components/thumbnail-list/thumbnail-list.component.ts ***!
  \***********************************************************************/
/*! exports provided: ThumbnailListComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ThumbnailListComponent", function() { return ThumbnailListComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ThumbnailListComponent = /** @class */ (function () {
    function ThumbnailListComponent() {
    }
    ThumbnailListComponent.prototype.ngOnInit = function () {
    };
    ThumbnailListComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-thumbnail-list',
            template: __webpack_require__(/*! ./thumbnail-list.component.html */ "./src/app/components/thumbnail-list/thumbnail-list.component.html"),
            styles: [__webpack_require__(/*! ./thumbnail-list.component.css */ "./src/app/components/thumbnail-list/thumbnail-list.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], ThumbnailListComponent);
    return ThumbnailListComponent;
}());



/***/ }),

/***/ "./src/app/components/warrior-create/warrior-create.component.css":
/*!************************************************************************!*\
  !*** ./src/app/components/warrior-create/warrior-create.component.css ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/components/warrior-create/warrior-create.component.html":
/*!*************************************************************************!*\
  !*** ./src/app/components/warrior-create/warrior-create.component.html ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "  <div class=\"container\">\n    <div class=\"col-md-12\">\n      <h1>Guerreiro: {{ name }}</h1>\n    </div>\n    \n    <div class=\"row\">\n      <div class=\"col-md-4\">\n        <div class=\"form-group\">\n          <label for=\"name\">Nome </label>\n          <input type=\"text\" id=\"name\" placeholder=\"Entre com o nome\" class=\"form-control\" [(ngModel)]=\"warrior.name\">\n        </div>\n      </div>\n\n      <div class=\"col-md-2\">\n        <div class=\"form-group\">\n          <label for=\"name\">Vida </label>\n          <input type=\"text\" id=\"life\" placeholder=\"\" class=\"form-control\" [(ngModel)]=\"warrior.life\">\n        </div>\n      </div>\n\n      <div class=\"col-md-3\">\n        <div class=\"form-group\">\n          <label for=\"name\">Tipo do Guerreiro </label>\n          <select id=\"tipo\" [(ngModel)]=\"warrior.category_id\" class=\"form-control form-control\">\n            <option *ngFor=\"let category of categories;\" value=\"{{ category.id }}\"\n             selected=\"{{ category.id == warrior.category_id ? true : 'false'  }}\" \n            >{{ category.name }}</option>\n          </select>\n        </div>\n      </div>\n      \n      <div class=\"col-md-3\">\n        <div class=\"form-group\">\n          <label for=\"name\">Status </label>\n          <select id=\"status\" [(ngModel)]=\"warrior.status\" class=\"form-control form-control\">\n            <option value=\"1\">Ativo</option>\n            <option value=\"0\">Inativo</option>\n          </select>\n        </div>\n      </div>\n    </div>\n    \n    <div class=\"row\">\n      <div class=\"col-md-3\">\n        <div class=\"form-group\">\n          <label for=\"name\">Velocidade de Movimento </label>\n          <input type=\"number\" [(ngModel)]=\"warrior.movement_velocity\" id=\"movement_velocity\" placeholder=\"velocidade de movimento\" class=\"form-control\" [(ngModel)]=\"movement_velocity\">\n        </div>\n      </div>\n      \n      <div class=\"col-md-3\">\n        <div class=\"form-group\">\n          <label for=\"name\">Dano </label>\n          <input type=\"number\" [(ngModel)]=\"warrior.damnification\" placeholder=\"0\" class=\"form-control\" value=\"0\" [(ngModel)]=\"damnification\">\n        </div>\n      </div>\n      <div class=\"col-md-3\">\n      <div class=\"form-group\">\n        <label for=\"name\"> Velocidade de ataque </label>\n        <input type=\"number\" [(ngModel)]=\"warrior.attack_velocity\" placeholder=\"Velocidade de ataque\" class=\"form-control\" [(ngModel)]=\"attack_velocity\" >\n      </div>\n    </div>\n    \n    <div class=\"col-md-3\">\n      <div class=\"form-group\">\n        <label for=\"name\">Defesa </label>\n        <input type=\"number\" id=\"defense\" placeholder=\"Defesa\" class=\"form-control\" [(ngModel)]=\"warrior.defense\">\n      </div>\n    </div>\n  </div>\n  \n  <div class=\"row\">\n    \n    <div class=\"col-md-4\">\n      <div class=\"form-group\">\n        <label for=\"speciality\">Especialidades {{ warrior.id_specialities|json }} </label>\n        <select id=\"speciality\" [(ngModel)]=\"warrior.specialities\" class=\"form-control form-control\" multiple>\n          <option *ngFor=\"let speciality of specialities;\" value=\"{{ speciality.id }}\"\n          selected=\"true\" checked=\"true\"\n          >{{ speciality.name }}</option>\n        </select>\n      </div>\n    </div>\n\n    <div class=\"col-md-6\" *ngIf=\"!warrior.id\">\n      <div class=\"form-group\">\n        <label for=\"type\" class=\"form-label\">Thumbnails </label>\n        <input class=\"form-control-file\"  placeholder=\"Selecione os thumbnails \" type=\"file\" id=\"file\" (change)=\"handleFiles($event.target.files)\" multiple> \n      </div>\n    </div>\n  </div>\n  \n  <div class=\"col-md-6\">\n    <button type=\"submit\" class=\"btn btn-lg btn-primary\" (click)=\"save()\">Gravar</button>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/components/warrior-create/warrior-create.component.ts":
/*!***********************************************************************!*\
  !*** ./src/app/components/warrior-create/warrior-create.component.ts ***!
  \***********************************************************************/
/*! exports provided: WarriorCreateComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WarriorCreateComponent", function() { return WarriorCreateComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_warriors_warrior_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../services/warriors/warrior.service */ "./src/app/services/warriors/warrior.service.ts");
/* harmony import */ var _services_specialities_speciality_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/specialities/speciality.service */ "./src/app/services/specialities/speciality.service.ts");
/* harmony import */ var _services_categories_category_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/categories/category.service */ "./src/app/services/categories/category.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var WarriorCreateComponent = /** @class */ (function () {
    function WarriorCreateComponent(warriorService, specialityService, categoryService, router, route) {
        this.warriorService = warriorService;
        this.specialityService = specialityService;
        this.categoryService = categoryService;
        this.router = router;
        this.route = route;
        this.warrior = {
            id: null,
            name: '',
            category_id: null,
            life: null,
            movement_velocity: null,
            damnification: null,
            attack_velocity: null,
            defense: null,
            specialities: null,
            status: null,
            thumbnails: [],
        };
        this.specialities = null;
        this.speciality = new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"]();
        this.categories = null;
        this.id_specialities = null;
    }
    WarriorCreateComponent.prototype.attachFiles = function (field, inputs, files) {
        for (var i = 0; i < files.length; i++) {
            inputs.append(field, files[i]);
        }
        return inputs;
    };
    WarriorCreateComponent.prototype.getInputs = function () {
        var inputs = new FormData();
        inputs.append('id', this.warrior.id ? this.warrior.id : '');
        inputs.append('name', this.warrior.name ? this.warrior.name : '');
        inputs.append('category_id', this.warrior.category_id ? this.warrior.category_id : '');
        inputs.append('life', this.warrior.life ? this.warrior.life : '');
        inputs.append('movement_velocity', this.warrior.movement_velocity ? this.warrior.movement_velocity : '');
        inputs.append('damnification', this.warrior.damnification ? this.warrior.damnification : '');
        inputs.append('attack_velocity', this.warrior.attack_velocity ? this.warrior.attack_velocity : '');
        inputs.append('defense', this.warrior.defense ? this.warrior.defense : '');
        inputs.append('specialities', this.warrior.specialities ? this.warrior.specialities : '');
        inputs.append('status', this.warrior.status ? this.warrior.status : '');
        if (this.files) {
            inputs = this.attachFiles('thumbnails[]', inputs, this.files);
        }
        return inputs;
    };
    WarriorCreateComponent.prototype.save = function () {
        var _this = this;
        var inputs = this.warrior.id ? this.warrior : this.getInputs();
        this.warriorService.save(inputs)
            .subscribe(function () { return _this.router.navigate(['/warriors']); });
    };
    WarriorCreateComponent.prototype.extractIdSpecialities = function (data) {
        return data.map(function (item) {
            return item.id;
        });
    };
    WarriorCreateComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.params.subscribe(function (params) {
            var self = _this;
            if (params.hasOwnProperty('id')) {
                _this.warriorService.find(+params['id'])
                    .subscribe(function (data) {
                    self.warrior = data.data;
                    self.id_specialities = self.extractIdSpecialities(self.warrior.specialities);
                });
            }
            // busca todos os tipos de Guerreiros, (estou chamando de Categorias)
            _this.categoryService.all()
                .subscribe(function (data) { return _this.specialities = data.data; });
            // 
            _this.specialityService.all()
                .subscribe(function (data) { return _this.categories = data.data; });
        });
    };
    WarriorCreateComponent.prototype.handleFiles = function (files) {
        this.warrior.thumbnails = files;
        this.files = files;
    };
    WarriorCreateComponent.prototype.getWarrior = function (id) {
        var _this = this;
        this.warriorService.get(id)
            .subscribe(function (data) { return _this.warrior = data; });
    };
    WarriorCreateComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-warrior-create',
            template: __webpack_require__(/*! ./warrior-create.component.html */ "./src/app/components/warrior-create/warrior-create.component.html"),
            styles: [__webpack_require__(/*! ./warrior-create.component.css */ "./src/app/components/warrior-create/warrior-create.component.css")]
        }),
        __metadata("design:paramtypes", [_services_warriors_warrior_service__WEBPACK_IMPORTED_MODULE_1__["WarriorService"],
            _services_specialities_speciality_service__WEBPACK_IMPORTED_MODULE_2__["SpecialityService"],
            _services_categories_category_service__WEBPACK_IMPORTED_MODULE_3__["CategoryService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"]])
    ], WarriorCreateComponent);
    return WarriorCreateComponent;
}());



/***/ }),

/***/ "./src/app/components/warrior-list/warrior-list.component.css":
/*!********************************************************************!*\
  !*** ./src/app/components/warrior-list/warrior-list.component.css ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/components/warrior-list/warrior-list.component.html":
/*!*********************************************************************!*\
  !*** ./src/app/components/warrior-list/warrior-list.component.html ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\n  <div class=\"col-md-12\">\n    <h1>Guerreiros</h1>\n  </div>\n  <br />\n\n  <div class=\"col-md-12\">\n    <Button class=\"btn btn-primary \" [routerLink]=\"['/warriors/create']\">Novo Guerreiro</Button>\n  </div>\n  <br />\n\n  <div class=\"col-md-3 card\" *ngFor=\"let warrior of warriors\">\n    <div class=\"card-body\">\n      <img src=\"{{ warrior.thumbnail.url }}\" alt=\"{{ warrior.category.name }}\" class=\"img-thumbnail\">\n      <h3>\n        <a></a>\n        <p class=\"text-uppercase\">{{ warrior.name }}</p>\n      </h3>\n\n      <span>\n        <a class=\"btn btn-md btn-info\" [routerLink]=\"['/warriors', warrior.id, 'show']\">Ver</a>|\n        <a class=\"btn btn-md btn-danger\" (click)=\"destroy(warrior.id)\" >Excluir</a>|\n        <a class=\"btn btn-md btn-warning\" [routerLink]=\"['/warriors', warrior.id, 'edit']\">Editar</a>\n      </span>\n    </div>\n  </div>\n\n</div>\n"

/***/ }),

/***/ "./src/app/components/warrior-list/warrior-list.component.ts":
/*!*******************************************************************!*\
  !*** ./src/app/components/warrior-list/warrior-list.component.ts ***!
  \*******************************************************************/
/*! exports provided: WarriorListComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WarriorListComponent", function() { return WarriorListComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_warriors_warrior_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../services/warriors/warrior.service */ "./src/app/services/warriors/warrior.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var WarriorListComponent = /** @class */ (function () {
    function WarriorListComponent(warriorService, router, route) {
        this.warriorService = warriorService;
        this.router = router;
        this.route = route;
        this.warriors = [];
    }
    WarriorListComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.params.subscribe(function (params) {
            _this.warriorService.all()
                .subscribe(function (data) { return _this.warriors = data.data; });
        });
    };
    WarriorListComponent.prototype.destroy = function (id, index) {
        var _this = this;
        if (confirm("tem certeza ?")) {
            this.warriorService.destroy(+id).subscribe(function () { return _this.router.navigate(['/warriors']); });
        }
    };
    WarriorListComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-warrior-list',
            template: __webpack_require__(/*! ./warrior-list.component.html */ "./src/app/components/warrior-list/warrior-list.component.html"),
            styles: [__webpack_require__(/*! ./warrior-list.component.css */ "./src/app/components/warrior-list/warrior-list.component.css")]
        }),
        __metadata("design:paramtypes", [_services_warriors_warrior_service__WEBPACK_IMPORTED_MODULE_1__["WarriorService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]])
    ], WarriorListComponent);
    return WarriorListComponent;
}());



/***/ }),

/***/ "./src/app/components/warrior-show/warrior-show.component.css":
/*!********************************************************************!*\
  !*** ./src/app/components/warrior-show/warrior-show.component.css ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".btn-return {\n    margin-left: 93%;\n}"

/***/ }),

/***/ "./src/app/components/warrior-show/warrior-show.component.html":
/*!*********************************************************************!*\
  !*** ./src/app/components/warrior-show/warrior-show.component.html ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\n  <div class=\"col-md-12\">\n    <h1>Guerreiro {{ warrior?.name }}</h1>\n  </div>\n  <button class=\"btn btn-primary btn-return\" [routerLink]=\"['/warriors']\">Voltar</button>\n  <hr />\n  <div class=\"pull-right row\">\n    <div class=\"col-xs-4 col-md-5\" *ngFor=\"let thumbnail of warrior.thumbnails;\" class=\"thumbnails\">\n      <a href=\"#\" class=\"thumbnail\">\n        <img src=\"{{ thumbnail.url }}\" height=\"150\" width=\"150\" alt=\"{{ thumbnail.name }}\" class=\"img-thumbnail\">\n      </a>\n    </div>\n  </div>\n\n  <div class=\"col-md-12\">\n    <div class=\"form-group\">\n      <strong> Nome : </strong> {{ warrior.name }}\n      <br>\n      <strong> Tipo : </strong> {{ warrior.category?.name }}\n      <br>\n      <strong>life: </strong> {{ warrior?.life}}\n      <br>\n      <strong>damnification: </strong> {{ warrior?.damnification}}\n      <br>\n      <strong>Defesa: </strong> {{ warrior?.defense}}\n      <br>\n      <strong>Velocidade de Ataque: </strong> {{ warrior?.attack_velocity}}\n      <br>\n      <strong>Velocidade de Movimento: </strong> {{ warrior?.movement_velocity}}\n      <br>\n      <strong>status: </strong>\n      <label *ngIf=\"warrior.status == 1;  else statusInativo\">\n        Ativo\n      </label>\n      <ng-template #statusInativo>\n        Inativo\n      </ng-template>\n\n      <br>\n      <strong>Data da última Alteração: </strong> {{ warrior?.status}}\n      <br>\n      <strong>Data de Cadastro: </strong> {{ warrior?.status}}\n      <br>\n      <strong>Especialidades: </strong>\n      <label class=\"\" *ngFor=\"let speciality of warrior.specialities;\">\n        {{ speciality.name }},\n      </label>\n\n    </div>\n  </div>\n\n\n  <div>\n  </div>\n\n</div>\n"

/***/ }),

/***/ "./src/app/components/warrior-show/warrior-show.component.ts":
/*!*******************************************************************!*\
  !*** ./src/app/components/warrior-show/warrior-show.component.ts ***!
  \*******************************************************************/
/*! exports provided: WarriorShowComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WarriorShowComponent", function() { return WarriorShowComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_warriors_warrior_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../services/warriors/warrior.service */ "./src/app/services/warriors/warrior.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var WarriorShowComponent = /** @class */ (function () {
    function WarriorShowComponent(warriorService, router, route) {
        this.warriorService = warriorService;
        this.router = router;
        this.route = route;
        this.warrior = {};
    }
    WarriorShowComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.params.subscribe(function (params) {
            if (params.hasOwnProperty('id')) {
                _this.warriorService.find(+params['id'])
                    .subscribe(function (data) { return _this.warrior = data.data; });
            }
        });
    };
    WarriorShowComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-warrior-show',
            template: __webpack_require__(/*! ./warrior-show.component.html */ "./src/app/components/warrior-show/warrior-show.component.html"),
            styles: [__webpack_require__(/*! ./warrior-show.component.css */ "./src/app/components/warrior-show/warrior-show.component.css")]
        }),
        __metadata("design:paramtypes", [_services_warriors_warrior_service__WEBPACK_IMPORTED_MODULE_1__["WarriorService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]])
    ], WarriorShowComponent);
    return WarriorShowComponent;
}());



/***/ }),

/***/ "./src/app/services/categories/category.service.ts":
/*!*********************************************************!*\
  !*** ./src/app/services/categories/category.service.ts ***!
  \*********************************************************/
/*! exports provided: CategoryService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CategoryService", function() { return CategoryService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _common_base_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../common/base.service */ "./src/app/services/common/base.service.ts");
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


var CategoryService = /** @class */ (function (_super) {
    __extends(CategoryService, _super);
    function CategoryService() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    CategoryService.prototype.all = function () {
        return this.http.get(this.api_BaseUrl + '/categories');
    };
    CategoryService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        })
    ], CategoryService);
    return CategoryService;
}(_common_base_service__WEBPACK_IMPORTED_MODULE_1__["BaseService"]));



/***/ }),

/***/ "./src/app/services/common/base.service.ts":
/*!*************************************************!*\
  !*** ./src/app/services/common/base.service.ts ***!
  \*************************************************/
/*! exports provided: BaseService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BaseService", function() { return BaseService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var BaseService = /** @class */ (function () {
    function BaseService(http) {
        this.http = http;
        this.baseUrl = 'http://localhost:8000';
        this.api_BaseUrl = 'http://localhost:8000/api';
    }
    BaseService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], BaseService);
    return BaseService;
}());



/***/ }),

/***/ "./src/app/services/specialities/speciality.service.ts":
/*!*************************************************************!*\
  !*** ./src/app/services/specialities/speciality.service.ts ***!
  \*************************************************************/
/*! exports provided: SpecialityService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SpecialityService", function() { return SpecialityService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _common_base_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../common/base.service */ "./src/app/services/common/base.service.ts");
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


var SpecialityService = /** @class */ (function (_super) {
    __extends(SpecialityService, _super);
    function SpecialityService() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    SpecialityService.prototype.all = function () {
        return this.http.get(this.api_BaseUrl + '/specialities');
    };
    SpecialityService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        })
    ], SpecialityService);
    return SpecialityService;
}(_common_base_service__WEBPACK_IMPORTED_MODULE_1__["BaseService"]));



/***/ }),

/***/ "./src/app/services/warriors/warrior.service.ts":
/*!******************************************************!*\
  !*** ./src/app/services/warriors/warrior.service.ts ***!
  \******************************************************/
/*! exports provided: WarriorService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WarriorService", function() { return WarriorService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var WarriorService = /** @class */ (function () {
    function WarriorService(http) {
        this.http = http;
        this.baseUrl = 'http://localhost:8000';
        this.api_BaseUrl = 'http://localhost:8000/api';
    }
    WarriorService.prototype.save = function (data) {
        if (data.id) {
            return this.http.put(this.api_BaseUrl + '/warriors/' + data.id, data);
        }
        return this.http.post(this.api_BaseUrl + '/warriors', data);
    };
    WarriorService.prototype.update = function (data) {
        return this.http.put(this.api_BaseUrl + '/warriors/' + data.id, data);
    };
    WarriorService.prototype.find = function (id) {
        return this.http.get(this.api_BaseUrl + '/warriors/' + id);
    };
    WarriorService.prototype.all = function () {
        return this.http.get(this.api_BaseUrl + '/warriors');
    };
    WarriorService.prototype.get = function (id) {
        return this.http.get(this.api_BaseUrl + '/warriors/' + id);
    };
    WarriorService.prototype.destroy = function (id) {
        return this.http.delete(this.api_BaseUrl + '/warriors/' + id);
    };
    WarriorService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], WarriorService);
    return WarriorService;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false
};
/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /home/osvaldoabel/projects/nw-warriors/spa/src/main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map