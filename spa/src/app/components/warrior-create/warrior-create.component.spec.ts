import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WarriorCreateComponent } from './warrior-create.component';

describe('WarriorCreateComponent', () => {
  let component: WarriorCreateComponent;
  let fixture: ComponentFixture<WarriorCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WarriorCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WarriorCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
