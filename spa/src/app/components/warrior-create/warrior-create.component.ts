import { Component, OnInit } from '@angular/core';
import { WarriorService } from '../../services/warriors/warrior.service';
import { SpecialityService } from '../../services/specialities/speciality.service';
import { CategoryService } from '../../services/categories/category.service';
import { ActivatedRoute, Router } from '@angular/router';

import { FormsModule } from '@angular/forms';
import { MatFormFieldModule, MatInputModule } from '@angular/material';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-warrior-create',
  templateUrl: './warrior-create.component.html',
  styleUrls: ['./warrior-create.component.css']
})
export class WarriorCreateComponent implements OnInit {
  warrior = <any>{
    id: null,
    name: '',
    category_id: null,
    life: null,
    movement_velocity: null,
    damnification: null,
    attack_velocity: null,
    defense: null,
    specialities: null,
    status: null,
    thumbnails: <any>[],
  };

  files: null;

  specialities = null;
  speciality = new FormControl();
  categories = null;
  id_specialities = null;

  constructor(
    private warriorService: WarriorService,
    private specialityService: SpecialityService,
    private categoryService: CategoryService,
    private router: Router,
    private route: ActivatedRoute
  ) {

  }

  attachFiles(field, inputs, files) {

    for (let i = 0; i < files.length; i++) {
      inputs.append(field, files[i]);
    }

    return inputs;
  }

  getInputs() {

    let inputs = new FormData();

    inputs.append('id', this.warrior.id ? this.warrior.id : '');
    inputs.append('name', this.warrior.name ? this.warrior.name : '');
    inputs.append('category_id', this.warrior.category_id ? this.warrior.category_id : '');
    inputs.append('life', this.warrior.life ? this.warrior.life : '');
    inputs.append('movement_velocity', this.warrior.movement_velocity ? this.warrior.movement_velocity : '');
    inputs.append('damnification', this.warrior.damnification ? this.warrior.damnification : '');
    inputs.append('attack_velocity', this.warrior.attack_velocity ? this.warrior.attack_velocity : '');
    inputs.append('defense', this.warrior.defense ? this.warrior.defense : '');
    inputs.append('specialities', this.warrior.specialities ? this.warrior.specialities : '');
    inputs.append('status', this.warrior.status ? this.warrior.status : '');

    if (this.files) {
      inputs = this.attachFiles('thumbnails[]', inputs, this.files);
    }

    return inputs;
  }

  save() {
    const inputs = this.warrior.id ? this.warrior : this.getInputs();
    this.warriorService.save(inputs)
      .subscribe(() => this.router.navigate(['/warriors']));
  }

  extractIdSpecialities(data) {
    return data.map(function (item) {
      return item.id;
    });
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      let self = this;

      if (params.hasOwnProperty('id')) {
        this.warriorService.find(+params['id'])
          .subscribe(function (data) {
            self.warrior = data.data
            self.id_specialities = self.extractIdSpecialities(self.warrior.specialities);
          });
      }

      // busca todos os tipos de Guerreiros, (estou chamando de Categorias)
      this.categoryService.all()
        .subscribe(data => this.specialities = data.data);

      // 
      this.specialityService.all()
        .subscribe(data => this.categories = data.data);
    });

  }

  handleFiles(files) {
    this.warrior.thumbnails = files;
    this.files = files;
  }

  getWarrior(id) {
    this.warriorService.get(id)
      .subscribe(data => this.warrior = data);
  }

}
