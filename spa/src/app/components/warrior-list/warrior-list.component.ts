import { Component, OnInit } from '@angular/core';
import { WarriorService } from '../../services/warriors/warrior.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-warrior-list',
  templateUrl: './warrior-list.component.html',
  styleUrls: ['./warrior-list.component.css']
})
export class WarriorListComponent implements OnInit {

  warriors = [];

  constructor(
    private warriorService: WarriorService,
    private router: Router,
    private route: ActivatedRoute
  ) {

  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.warriorService.all()
        .subscribe((data) => this.warriors = data.data);
    });
  }

  destroy(id, index) {
		if (confirm("tem certeza ?")) {
			this.warriorService.destroy(+id).subscribe(() => this.router.navigate(['/warriors']));
		}
  }
  
}
