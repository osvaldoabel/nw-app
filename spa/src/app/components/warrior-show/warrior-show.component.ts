import { Component, OnInit } from '@angular/core';
import { WarriorService } from '../../services/warriors/warrior.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-warrior-show',
  templateUrl: './warrior-show.component.html',
  styleUrls: ['./warrior-show.component.css']
})
export class WarriorShowComponent implements OnInit {

  warrior = {};

  constructor(
    private warriorService: WarriorService,
    private router: Router,
    private route: ActivatedRoute
  ) {

  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      if (params.hasOwnProperty('id')) {
        this.warriorService.find(+params['id'])
          .subscribe(data => this.warrior = data.data);
      }
    });
  }
}
