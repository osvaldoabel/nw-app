import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WarriorShowComponent } from './warrior-show.component';

describe('WarriorShowComponent', () => {
  let component: WarriorShowComponent;
  let fixture: ComponentFixture<WarriorShowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WarriorShowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WarriorShowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
