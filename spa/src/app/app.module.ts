import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { WarriorListComponent } from './components/warrior-list/warrior-list.component';
import { WarriorCreateComponent } from './components/warrior-create/warrior-create.component';
import { WarriorService } from './services/warriors/warrior.service';
import { Routes, RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { WarriorShowComponent } from './components/warrior-show/warrior-show.component';
import { ThumbnailListComponent } from './components/thumbnail-list/thumbnail-list.component';
import { MatSelectModule } from '@angular/material/select';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

const routes = [
  { path: '', component: WarriorListComponent },
  { path: 'warriors', component: WarriorListComponent },
  { path: 'warriors/:id/show', component: WarriorShowComponent },
  { path: 'warriors/create',   component: WarriorCreateComponent },
  { path: 'warriors/:id/edit', component: WarriorCreateComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    WarriorListComponent,
    WarriorCreateComponent,
    WarriorShowComponent,
    ThumbnailListComponent
  ],

  imports: [
    BrowserModule,
    RouterModule.forRoot(routes, { useHash: true }),
    HttpClientModule,
    FormsModule,
    MatSelectModule,
    BrowserAnimationsModule,
    ReactiveFormsModule
  ],
  providers: [WarriorService],
  bootstrap: [AppComponent]
})
export class AppModule { }
