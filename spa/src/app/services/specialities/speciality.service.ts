import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { BaseService} from '../common/base.service';

@Injectable({
  providedIn: 'root'
})
export class SpecialityService extends BaseService{

  all(): Observable<any> {
    return this.http.get<any>(this.api_BaseUrl+'/specialities');
  }
}