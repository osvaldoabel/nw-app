import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class BaseService {
  
  protected baseUrl     = 'http://localhost:8000';
  protected api_BaseUrl  = 'http://localhost:8000/api';

  constructor(protected http:HttpClient) { }

}