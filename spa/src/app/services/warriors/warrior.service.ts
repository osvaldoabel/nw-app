import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

@Injectable({
  providedIn: 'root'
})
export class WarriorService {
  
  private baseUrl      = 'http://localhost:8000';
  private api_BaseUrl  = 'http://localhost:8000/api';

  constructor(private http:HttpClient) { }

  save(data:any): Observable<any> {

    if (data.id) {
      return this.http.put(this.api_BaseUrl+ '/warriors/'+data.id, data);
    }

  	return this.http.post(this.api_BaseUrl+ '/warriors', data);
  }

  update(data:any): Observable<any> {
    return this.http.put(this.api_BaseUrl+ '/warriors/'+data.id, data);    
  }

  find(id:number): Observable<any> {
  	return this.http.get(this.api_BaseUrl+'/warriors/'+id);
  }

  all(): Observable<any> {
    return this.http.get<any>(this.api_BaseUrl+'/warriors');
  }

  get(id: number) {
    return this.http.get(this.api_BaseUrl + '/warriors/'+id);
  }

  destroy(id: number) {
    return this.http.delete(this.api_BaseUrl+'/warriors/'+id);
  }


}


