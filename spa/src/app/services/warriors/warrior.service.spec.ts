import { TestBed, inject } from '@angular/core/testing';

import { WarriorService } from './warrior.service';

describe('WarriorService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [WarriorService]
    });
  });

  it('should be created', inject([WarriorService], (service: WarriorService) => {
    expect(service).toBeTruthy();
  }));
});
